package com.first.actors

import spray.json._

case class SlackMessage(token: String, team_id :String, team_domain :String,
                        channel_id: String, channel_name: String, timestamp: String,
                        user_id:String, user_name: String, text: String, trigger_word:String)
case class CurrencyApiResult[T](terms: String, success: Boolean, source: String,
                                      timestamp: Double, privacy: String, quotes: Map[String, Double])
case class SlackResponse(channel_name: String, user_name: String, text: String)
case class SlackResponseContent(username: String, channel: String, text: String)
case class SlackResponseFormat(payload: SlackResponseContent)

object SlackMessageProtocol extends DefaultJsonProtocol {
  implicit val slackMessageFormat = jsonFormat10(SlackMessage)
  implicit def currencyApiResultFormat[T :JsonFormat] = jsonFormat6(CurrencyApiResult.apply[T])
  implicit val slackResponseContentFormat = jsonFormat3(SlackResponseContent)
  implicit val slackResponseMessageFormat = jsonFormat1(SlackResponseFormat)
}
