package com.first.actors

import akka.actor.{Actor, Props}
import com.first.actors.SlackMessageProtocol._
import spray.httpx.SprayJsonSupport._
import spray.routing._

class SprayActor extends Actor with HttpService {
  def actorRefFactory = context
  def receive = runRoute(defaultRoute)

  val defaultRoute =
    post {
        path("slack") {
          entity(as[SlackMessage]) { slackMessage => requestContext =>
                val slackMessageService = actorRefFactory.actorOf((Props(new SlackMessageService(requestContext))))
                val slackSenderService = actorRefFactory.actorOf(Props(new SlackSenderService()))
                slackMessageService ! SlackMessageService.Process(slackMessage, slackSenderService)
          }
        }
      }
}