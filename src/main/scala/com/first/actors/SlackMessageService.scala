package com.first.actors


import akka.actor.{Actor, ActorRef}
import akka.event.Logging
import com.first.actors.SlackMessageProtocol._
import com.first.actors.SlackSenderService.sendMessage
import spray.client.pipelining._
import spray.httpx.SprayJsonSupport._
import spray.routing.RequestContext

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.util.{Failure, Properties, Success}


object SlackMessageService{
  case class Process(slackMessage: SlackMessage, sender: ActorRef)
}

class SlackMessageService(requestContext: RequestContext) extends Actor{
  import SlackMessageService._

  implicit val system = context.system
  val log = Logging(system, getClass)

  def receive = {
    case Process(slackMessage, sender) =>
      process(slackMessage, sender)
      context.stop(self)
  }

  def process(slackMessage : SlackMessage, sender : ActorRef) = {
    val apiKey = Properties.envOrElse("currency_key", "")

    log.info("Requesting : {}", slackMessage)
    log.info("api key : {}", apiKey)

    val pipeline = sendReceive ~> unmarshal[CurrencyApiResult[Map[String, Double]]]

    val responseFuture = pipeline{
      Get(s"http://www.apilayer.net/api/live?access_key=$apiKey&currencies=AUD,EUR,TWD,JPY")
    }

    responseFuture onComplete {
      case Success(CurrencyApiResult(_, _, _, _, _, map)) =>
        log.info("The elevation is: {}", map)
        sender ! sendMessage(SlackResponse(slackMessage.channel_name, slackMessage.user_name, map.toString()))
        requestContext.complete("ok")

      case Failure(error) =>
        log.error("Look up currency failed, {}", error)
        requestContext.complete(error)
    }
  }
}


