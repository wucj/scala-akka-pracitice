package com.first.actors

import akka.actor.Actor
import akka.event.Logging
import spray.client.pipelining._
import spray.http.HttpRequest
import com.first.actors.SlackMessageProtocol._
import spray.httpx.SprayJsonSupport._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.util.{Failure, Properties, Success}


object SlackSenderService{
  case class sendMessage(message: SlackResponse)
}

class SlackSenderService extends Actor {

  import SlackSenderService._

  implicit val system = context.system
  val log = Logging(system, getClass)

  def receive = {
    case sendMessage(message) =>
      process(message)
      context.stop(self)
  }

  def process(message: SlackResponse) = {
    val slackUrl = Properties.envOrElse("slack_url", "")
    val postData = SlackResponseFormat(SlackResponseContent("test_bot", message.channel_name, message.text))

    log.info("Requesting : {}", message)
    log.info("Slack Url : {}", slackUrl)

    val pipeline : HttpRequest => Future[String] = sendReceive ~> unmarshal[String]

    val responseFuture : Future[String] =
      pipeline(Post(s"$slackUrl", postData))

    responseFuture onComplete {
      case Success(_) => log.info("Send message to slack.")
      case Failure(error) => log.info("Failed to send message to slack {}", error)
    }

  }
}
