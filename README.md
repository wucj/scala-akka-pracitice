## scala-slack-currency-bot

This is a simple scala-akka application. The main purpose of this appplication is that receive message from slack with specific message format and then send back the result of live currency to the same channel of slack.

## Steps of development and test

Before we start to implement the scala-akka, some prework should be fininshed.

1. How to send message from slack. We need to setup the [outgoing webhook](https://api.slack.com/outgoing-webhooks).

2. We should have a web service to receive message from the slack, hance, I use [spray](http://spray.io/) as framework to create a web services.

3. The simplest way to create a web service is to implemente the [spray-route](http://spray.io/documentation/1.2.2/spray-routing/)

4. We could run the web service on the local by using sbt

5. Simple test on the local via curl command
```
$ curl \
    --request POST \
    --data {"token": "XXXXXXXXXXXXXXXXXX","team_id": "T0001","team_domain": "example","channel_id": "C2147483705","channel_name": "#random","timestamp": "1355517523.000005","user_id": "U2147483697","user_name": "Steve","text": "mybot: USD to JPN,TWD","trigger_word": "mybot:"} \
    http://localhost:8080/slack
```
6. Find an API to help us knwo the live currency

7. Implement the http GET from the API

8. Before the local test, please export the currency_key as system environment
```
$ export currency_key=API_KEY
```
9. Check the result from the currency API as our expectation

10. Next, we should put the result back to the slack channel, so, we need to apply [incoming webhooks](https://api.slack.com/incoming-webhooks), this is the simplest way to send message to the channel.

11. Implement the http POST via url which generated from slack.

12. Before the local test, please also export the slack_url as system environment
```
$ export slack_url=https://incoming-slack-url
```
13. Finish the test and check the result in the slack channel

## Improvment

This is the simple practice of message sending between akka actor, however, we do have a lot of things not done yet.

ex. more flexible currency lookup, error handling...
